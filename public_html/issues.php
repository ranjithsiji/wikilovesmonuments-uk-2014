<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;

print get_common_header ( '' , 'WLM UK issues' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;

function mergeDupes ( $qs ) {
	global $dbwd ;
	if ( count ( $qs ) != 2 ) return false ;
	
	$counts = array() ;
	foreach ( $qs AS $num => $q ) {
		$sql = "SELECT count(*) as cnt FROM pagelinks WHERE pl_namespace=0 and pl_from_namespace=0 and pl_title='q$q'" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
		$o = $result->fetch_object() ;
		$counts[$num] = $o->cnt ;
	}
	if ( $counts[0] != 0 or $counts[1] != 0 ) return false ;

	$titles = array() ;
	foreach ( $qs AS $num => $q ) {
		$sql = "SELECT term_text FROM wb_terms WHERE term_entity_id=$q AND term_entity_type='item' and term_language='en' and term_type='label'" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
		$o = $result->fetch_object() ;
		$titles[$num] = trim ( strtolower ( $o->term_text ) ) ;
	}
	if ( $titles[0] != $titles[1] ) return false ;
	
//	print "<li>Could merge the following?</li>" ;
	
	return true ;
}

function run () {
	global $dbwd ;
	global $db , $settings , $wdq_internal_url ;
	$main_prop = $settings['main_prop'] ;
	$country = $settings['country'] ;
	$use_id_column = $settings['use_id_column'] ;

	$only = array() ;
	$sql = "select $use_id_column from entry,region where entry.region=region.id and country='$country'" ; // All IDs for this country
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$only[$o->$use_id_column] = 1 ;
	}
	
	$onlyq = array() ;
	$sql = "SELECT page_title FROM page,pagelinks WHERE page_id=pl_from AND pl_namespace=120 and pl_from_namespace=0 and pl_title='P$main_prop'" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	while($o = $result->fetch_object()){
		$onlyq[preg_replace('/\D/','',$o->page_title)*1] = 1 ;
	}
	
	

	print "<h1>Issues with $country</h1>" ;
	
	$url = "$wdq_internal_url?q=claim[$main_prop]&props=$main_prop" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$multi_q = array() ;
	$multi_id = array() ;
	foreach ( $j->props->$main_prop AS $v ) {
		if ( !isset($only[$v[2]]) ) continue ;
		if ( !isset($onlyq[$v[0]]) ) continue ;
		$multi_q[$v[0]][] = $v[2] ;
		$multi_id[$v[2]][] = $v[0] ;
	}
	
	
	print "<h2>Items having multiple $country IDs</h2>" ;
	print "<p>Resolve: Create or find second item; assign ID; remove ID from first item; in wlmuk DB, add new Q item to wikidata table, fix b2q entry</p>" ;
	print "<ol>" ;
	foreach ( $multi_q AS $q => $v ) {
		if ( count($v) == 1 ) continue ;
		print "<li>" ;
		print "<a href='//www.wikidata.org/wiki/Q$q' target='_blank'><b>Q$q</b></a> : " ;
		foreach ( $v AS $a => $b ) {
			if ( $a > 0 ) print ", " ;
			print $b ;
		}
		print "</li>" ;
	}
	print "</ol>" ;

	$to_merge = array() ;
	print "<h2>$country IDs with multiple items</h2>" ;
	print "<ol>" ;
	foreach ( $multi_id AS $id => $v ) {
		if ( count($v) == 1 ) continue ;
		if ( mergeDupes ( $v ) ) {
			if ( $v[0]*1 > $v[1]*1 ) $v = array ( $v[1] , $v[0] ) ;
			$to_merge[] = "MERGE\tQ" . $v[0] . "\tQ" . $v[1] ;
			continue ;
		} ;
		print "<li>" ;
		print "<b>" ;
		if ( $country == 'England' ) print "<a href='http://list.english-heritage.org.uk/resultsingle.aspx?uid=$id' target='_blank'>$id</a>" ;
		else print $id ;
		print "</b> : " ;
		foreach ( $v AS $a => $q ) {
			if ( $a > 0 ) print "; " ;
			print "<a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a>" ;
		}
		print "</li>" ;
	}
	print "</ol>" ;
	if ( count ( $to_merge ) > 0 ) print "<h3>Merge candidates</h3><textarea rows=5 style='width:100%'>" . implode("\n",$to_merge) . "</textarea>" ;


	print "<h2>No Wikidata item in $country</h2>" ;
	print "<ol>" ;
	$sql = "select entry.id AS id,grade,name,$use_id_column from entry,region where entry.region=region.id and country='$country' AND grade IN ('" . implode("','",$settings['grades']) . "') and not exists (select * from b2q where building_id=entry.id)" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$url = "$wdq_internal_url?q=string[$main_prop:\"" . $o->$use_id_column . "\"]" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;

		if ( count($j->items) == 1 ) {
			$q = $j->items[0] ;
			$id = $o->id ;
			$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES ($q,$q)" ;
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");

			$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES ($q,$id,'wikidata')" ;
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");
			continue ;
		}
		
		print "<li>" ;
		print "<b>" . $o->$use_id_column . "</b>" ;
		if ( count($j->items) > 0 ) {
			print " | " ;
			foreach ( $j->items AS $a => $q ) {
				if ( $a > 0 ) print "; " ;
				print "<a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a>" ;
			}
			print " | " ;
		}
		print " <i>" . $o->name . "</i> [" . $o->grade . "]; internal entry ID #" . $o->id ;
		print "</li>" ;
	}
	print "</ol>" ;
	
}

$settings = array (
	'main_prop' => 709 ,
	'country' => 'Scotland' ,
	'use_id_column' => 'hbnum' ,
	'grades' => array('A')
) ;

run() ;

print "<hr/" ; //exit(0);

$settings = array (
	'main_prop' => 1216 ,
	'country' => 'England' ,
	'use_id_column' => 'ext_id' ,
	'grades' => array('I','II*')
) ;

run() ;

?>