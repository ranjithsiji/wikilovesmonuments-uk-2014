<?PHP

/*

TO UPDATE THE FULLTEXT SEARCH INDEX:

truncate search_region;
INSERT INTO search_region (region,text) select id,concat(country,'/',region,'/',district,'/',council,'/',parish,'/',community) from region;
truncate search_entry;
INSERT INTO search_entry (entry,text) select entry.id as id,concat(`name`,'/',location,'/',authority,'/',locality,'/',area,'/',park_name,'/',parish,'/',postcode,'/',listed_as,',',search_region.text) from entry,search_region WHERE entry.region=search_region.region;
truncate coords;
insert into coords (entry,latlon) SELECT id,point(latitude,longitude) from entry where latitude is not null;

*/


//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
require_once ( 'php/common.php' ) ;

header("Connection: close");
header('Content-type: application/json');
header("Cache-Control: no-cache, must-revalidate");

$db = openToolDB ( 'wlmuk_p' ) ;

$out = array ( 'status' => 'OK' , 'data' => array() ) ;
$action = get_request ( 'action' , '' ) ;

function addExistingMedia () {
	global $out , $db ;
	$ids = array_keys ( $out['data']['objects'] ) ;
	if ( count ( $ids ) == 0 ) return ;

	$sql = "SELECT entry.id AS eid,region.* FROM region,entry WHERE region.id=entry.region AND entry.id IN (" . implode(',',$ids) . ")" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	while($o = $result->fetch_object()){
		$out['data']['objects'][$o->eid]->region = $o ;
	}
	
	$sql = "SELECT * FROM existing_media WHERE entry IN (" . implode(',',$ids) . ")" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	while($o = $result->fetch_object()){
		if ( $o->image != '' ) $out['data']['objects'][$o->entry]->image = $o->image ;
		if ( $o->commons_cat != '' ) $out['data']['objects'][$o->entry]->commonscat = $o->commons_cat ;
	}
	
	$qs = array() ;
	foreach ( $out['data']['objects'] AS $k => $v ) {
		if ( !isset($v->q) ) continue ;
		$qs[$v->q] = $k ;
	}
	if ( count($qs) > 0 ) {
		$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
		$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='enwiki' AND ips_item_id IN (" . implode(',',array_keys($qs)) . ")" ;
		if(!$result = $dbwd->query($sql)) $out['status'] = "SQL error: " . $dbwd->error . ' : ' . $sql ;
		while($o = $result->fetch_object()){
			$id = $qs[$o->ips_item_id] ;
			$out['data']['objects'][$id]->enwp = $o->ips_site_page ;
		}
	}
}


if ( $action == 'search' ) {

	$out['data']['objects'] = array() ;
	$out['data']['locations'] = array() ;
	$query = $db->real_escape_string ( preg_replace ( '/[ ,;]+/' , '%' , trim ( get_request ( 'query' , '' ) ) ) ) ;
	
	if ( $query == '' ) $out['status'] = "ERROR: Empty search query" ;
	
	$sql = "SELECT entry.* FROM entry,search_entry WHERE search_entry.entry=entry.id AND text LIKE '%$query%'" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	$entry_ids = array() ;
	while($o = $result->fetch_object()){
		$entry_ids[] = $o->id ;
		$out['data']['objects'][$o->id] = $o ;
	}

	if ( count ( $entry_ids ) > 0 ) {
		$sql = "SELECT * FROM b2q WHERE building_id IN (" . implode(',',$entry_ids) . ")" ;
		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		while($o = $result->fetch_object()){
			$out['data']['objects'][$o->building_id]->q = $o->wikidata_id ;
		}
	}
	
	$sql = "SELECT region.* FROM region,search_region WHERE search_region.region=region.id AND text LIKE '%$query%'" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	while($o = $result->fetch_object()){
			$out['data']['locations'][$o->id] = $o ;
	}
	
	addExistingMedia() ;

} else if ( $action == 'in_location' ) {

	$out['data']['objects'] = array() ;
	$out['data']['locations'] = array() ;

	$lid = get_request ( 'location' , '' ) * 1 ;

	$sql = "SELECT * FROM entry WHERE region=$lid" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	$entry_ids = array() ;
	while($o = $result->fetch_object()){
		$entry_ids[] = $o->id ;
		$out['data']['objects'][$o->id] = $o ;
	}

	if ( count ( $entry_ids ) > 0 ) {
		$sql = "SELECT * FROM b2q WHERE building_id IN (" . implode(',',$entry_ids) . ")" ;
		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		while($o = $result->fetch_object()){
			$out['data']['objects'][$o->building_id]->q = $o->wikidata_id ;
		}
	}

	$sql = "SELECT * FROM region WHERE id=$lid" ;
	if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
	while($o = $result->fetch_object()){
		$out['data']['locations'][$o->id] = $o ;
	}

	addExistingMedia() ;

} else if ( $action == 'around' ) {

	$out['data']['objects'] = array() ;
	$out['data']['locations'] = array() ;
	
	$lat = get_request ( 'lat' , 0 ) * 1 ;
	$lon = get_request ( 'lon' , 0 ) * 1 ;
	$radius = get_request ( 'radius' , 0 ) * 1 ;
	
	if ( 1 ) {
		$radius /= 100 ;
		$sql = "select entry.*,st_distance(latlon,point($lat,$lon)) as dist from coords,entry where coords.entry=entry.id having dist<$radius order by dist" ;
		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		$entry_ids = array() ;
		while($o = $result->fetch_object()){
			$entry_ids[] = $o->id ;
			$out['data']['objects'][$o->id] = $o ;
		}
	} else {
		$sql = "SELECT *,acos(sin($lat) * sin(`latitude`) + cos($lat) * cos(`latitude`) * cos(`longitude` - ($lon))) * 6371 AS dist FROM `entry` HAVING dist <= $radius ORDER BY dist LIMIT 30" ;
/*
		$dist = $radius ;
		$rlon1 = $lon-$dist/abs(cos(deg2rad($lat))*69);
		$rlon2 = $lon+$dist/abs(cos(deg2rad($lat))*69);
		$rlat1 = $lat-($dist/69);
		$rlat2 = $lat+($dist/69);
		$sql = "select * from entry where ( latitude BETWEEN $rlat1 AND $rlat2 ) AND ( longitude BETWEEN $rlon1 AND $rlon2 )" ;
*/

		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		$entry_ids = array() ;
		while($o = $result->fetch_object()){
			$entry_ids[] = $o->id ;
			$out['data']['objects'][$o->id] = $o ;
		}
	}
	

	if ( count ( $entry_ids ) > 0 ) {
		$sql = "SELECT * FROM b2q WHERE building_id IN (" . implode(',',$entry_ids) . ")" ;
		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		while($o = $result->fetch_object()){
			$out['data']['objects'][$o->building_id]->q = $o->wikidata_id ;
		}
		
		$sql = "SELECT * FROM region WHERE EXISTS ( SELECT * FROM entry WHERE entry.region=region.id AND entry.id IN (" . implode(',',$entry_ids) . "))" ;
		if(!$result = $db->query($sql)) $out['status'] = "SQL error: " . $db->error . ' : ' . $sql ;
		while($o = $result->fetch_object()){
			$out['data']['locations'][$o->id] = $o ;
		}
	}

	addExistingMedia() ;

} else {
	$out['status'] = "ERROR: Unknown action \"$action\"." ;
}


print json_encode ( $out ) ;

?>