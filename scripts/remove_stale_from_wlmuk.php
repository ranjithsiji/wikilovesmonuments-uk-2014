#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;
$dbwd = openDB ( 'wikidata' , '' ) ;

function kill_em ( $country , $main_prop ) {
	global $db , $dbwd ;
	$qs = array() ;
	$sql = "SELECT DISTINCT wikidata_id FROM b2q,region,entry WHERE building_id=entry.id and entry.region=region.id and region.country='$country'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$qs[$o->wikidata_id] = $o->wikidata_id ;
	}


	$sql = "SELECT DISTINCT epp_entity_id FROM wb_entity_per_page,pagelinks where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=120 and pl_title='P$main_prop' and pl_from_namespace=0" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		unset ( $qs[$o->epp_entity_id] ) ;
	}


	print "Deleting " . count($qs) . " entries for $country\n" ;

	foreach ( $qs AS $q ) {
		$sql = "DELETE FROM b2q WHERE wikidata_id=$q" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	}
}

kill_em ( 'England' , 1216 ) ;
kill_em ( 'Scotland' , 709 ) ;

$sql = "DELETE from wikidata WHERE not exists (SELECT * FROM b2q where wikidata_id=wikidata.id)" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');



?>