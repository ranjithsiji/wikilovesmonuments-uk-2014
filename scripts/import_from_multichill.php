#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;

/*
$campaign = 'sct' ;
$main_prop = 709 ;
$country = 'Scotland' ;
$use_id_column = 'hbnum' ; // ext_id / hbnum
#$grade = 'A' ; $lbq = 'Q10729054' ; # Instance of: Grade A (Scotland)
$grade = 'B' ; $lbq = 'Q10729125' ; # Instance of: Grade B (Scotland)
$source_item = 'Q111854' ;
*/

$campaign = 'eng' ;
$main_prop = 1216 ;
$country = 'England' ;
$use_id_column = 'ext_id' ;
$source_item = 'Q936287' ;
#$grade = 'I' ; $lbq = 'Q15700818' ; # Instance of: Grade I listed building
$grade = 'II*' ; $lbq = 'Q15700831' ; # Instance of: Grade II* listed building

/*
$campaign = 'wls' ;
$main_prop = 1459 ;
$country = 'Wales' ;
$use_id_column = 'ext_id' ;
$grade = 'I' ; $lbq = 'Q15700818' ;
#$grade = 'II*' ; $lbq = 'Q15700831' ;
$source_item = 'Q15700831' ;
*/

/*
$campaign = 'nir' ;
$main_prop = 1460 ;
$country = 'Northern Ireland' ;
$use_id_column = 'hbnum' ;
$grade = 'A' ; $lbq = 'Q6646177' ;
#$grade = 'II*' ; $lbq = 'Q15700831' ;
$source_item = 'Q936287' ;
*/

# Sync country
$data = array() ;
if ( true ) {
	$sql = "select $use_id_column,wikidata_id,source,name,entry.id AS entry_id from entry,region,b2q where b2q.building_id=entry.id and entry.region=region.id and region.country='$country'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$data[$o->$use_id_column][strtolower($o->name)] = $o ;
	}
	$url = "$wdq_internal_url?q=claim[$main_prop]&props=$main_prop" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
}

//print count ( $j->items ) . " items found.\n" ;

function importLinks ( $prop , $j , $source , $country ) {
	global $data , $db , $use_id_column , $campaign ;
//	print "Trying " . count($j->props->$prop) . " entries...\n" ;
	foreach ( $j->props->$prop AS $v ) {
/*		if ( !preg_match ( '/^\d+$/' , $v[2] ) ) {
			print "Illegal value: " . implode("|",$v) . "\n" ;
			continue ;
		}*/
/*		if ( isset($data[$v[2]]) ) {
			if ( $data[$v[2]]->wikidata_id != $v[0] ) {
				print "Mismatch\n" ;
				#TODO mismatch
			}
			continue ;
		}*/

		$id = '' ;
		$sql = "select name,entry.id AS id from entry where $use_id_column='".$db->real_escape_string($v[2])."' AND campaign='$campaign'" ; //  type='monument' AND 
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
//		if ( $v[2] == '19940' ) print_r ( $data[$v[2]] ) ;
		while($o = $result->fetch_object()){
//			if ( $v[2] == '19940' ) print_r ( $o ) ;
			if ( isset($data[$v[2]]) and count($data[$v[2]]) > 1 ) {
//				if ( $v[2] == '19940' ) print "checking " .  strtolower($o->name) . "\n" ;
				if ( !isset($data[$v[2]][strtolower($o->name)]) ) continue ;
			}
			if ( $id == '' ) $id = $o->id ;
			else $id = '!' ;
		}

		if ( $id == '' ) {
	//		print "Could not find $country ID " . $v[2] . "\n" ;
			continue ;
		}
		if ( $id == '!' ) {
//			print "Multiple $country ID " . $v[2] . "\n" ;
			continue ;
		}
		
//		print "$id\n" ; continue ;

		$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES (" . $v[0] . "," . $v[0] . ")" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");
	
		$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES (" . $v[0] . ",$id,'$source')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");
//		print "$sql\n" ;
		
//		$data[$v[2]][] = array ( 'wikidata_id' => $v[0] , 'building_id' => $v[2] , 'source' => $source ) ;
	}
}

function importFromMultichill ( $table , $country , $id_col , $id_col2 ) {
	global $dbmc , $dbwd , $db ;
	$sql = "select $id_col,monument_article from `$table` where monument_article!=''" ; // subdivision_iso,type
	$page2id = array() ;
	if(!$result = $dbmc->query($sql)) die('There was an error running the query [' . $dbmc->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$k = str_replace ( '_' , ' ' , $o->monument_article ) ;
		$page2id[$k] = $o ;
	}
//	print count($page2id)."\n" ; exit ( 0 ) ;

	$tmp = array() ;
	foreach ( $page2id AS $k => $v ) {
		$tmp[] = $dbmc->real_escape_string ( $k ) ;
	}

	$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='enwiki' AND ips_site_page IN ('" . implode("','",$tmp) . "')" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwb->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		if ( !isset($page2id[$o->ips_site_page]) ) continue ;
		$page2id[$o->ips_site_page]->has_item = true ;
		$q = $o->ips_item_id ;

		$id = '' ;
		$uid = $page2id[$o->ips_site_page]->$id_col ;
		$sql = "select entry.id AS id from entry,region where $id_col2='$uid' AND entry.region=region.id and region.country='$country'" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		while($o = $result2->fetch_object()){
			if ( $id == '' ) $id = $o->id ;
			else $id = '!' ;
		}
		if ( $id == '' ) {
	//		print "Could not find $country ID " . $uid . "\n" ;
			continue ;
		}
		if ( $id == '!' ) {
			print "Multiple $country ID " . $uid . " [multichill]\n" ;
			continue ;
		}

		$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES ($q,$q)" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES ($q,'$id','multichill')" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	}

return ;
	foreach ( $page2id AS $k => $v ) {
		if ( isset($v->has_item) ) continue ;
		print "No item for [[$k]]\n" ;
	}
}


/*
function importFromMultichillViaURL ( $campaign ) { //$table , $country , $id_col , $id_col2 ) {
	global $dbmc , $dbwd , $db ;
	$table = "monuments_gb-" . $campaign . "_(en)" ;
	$sql = "select lat,lon,monument_article from `$table` where monument_article!=''" ;
	$loc2id = array() ;
	if(!$result = $dbmc->query($sql)) die('There was an error running the query [' . $dbmc->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$k = $o->lat."/".$o->lon ;
//		$k = str_replace ( '_' , ' ' , $o->monument_article ) ;
//		print "$v\n" ;
		$loc2id[$k] = $o ;
	}
	
	$sql = "SELECT * FROM entry WHERE campaign='$campaign'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$k = $o->latitude."/".$o->longitude ;
		if ( !isset($loc2id[$k]) ) continue ;
		print $o->name . "\n" ;
	}
	
	exit ( 0 ) ;
//	print count($page2id)."\n" ; exit ( 0 ) ;

	$tmp = array() ;
	foreach ( $page2id AS $k => $v ) {
		$tmp[] = $dbmc->real_escape_string ( $k ) ;
	}

	$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='enwiki' AND ips_site_page IN ('" . implode("','",$tmp) . "')" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwb->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		if ( !isset($page2id[$o->ips_site_page]) ) continue ;
		$page2id[$o->ips_site_page]->has_item = true ;
		$q = $o->ips_item_id ;

		$id = '' ;
		$uid = $page2id[$o->ips_site_page]->$id_col ;
		$sql = "select entry.id AS id from entry,region where $id_col2='$uid' AND entry.region=region.id and region.country='$country'" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		while($o = $result2->fetch_object()){
			if ( $id == '' ) $id = $o->id ;
			else $id = '!' ;
		}
		if ( $id == '' ) {
	//		print "Could not find $country ID " . $uid . "\n" ;
			continue ;
		}
		if ( $id == '!' ) {
			print "Multiple $country ID " . $uid . " [multichill]\n" ;
			continue ;
		}
	
		$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES ($q,$q)" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
		$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES ($q,'$id','multichill')" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	}

return ;
	foreach ( $page2id AS $k => $v ) {
		if ( isset($v->has_item) ) continue ;
		print "No item for [[$k]]\n" ;
	}
}
*/


//importLinks ( $main_prop , $j , 'wikidata' , $country ) ;
//exit(0) ;

$dbwd = openDB ( 'wikidata' , '' ) ;

$dbmc = openToolDB ( 'heritage_p' , '' , 's51138' ) ;
#importFromMultichill ( 'monuments_gb-eng_(en)' , 'England' , 'uid' , $use_id_column ) ;
#importFromMultichill ( 'monuments_gb-sct_(en)' , 'Scotland' , 'hb' , $use_id_column ) ;
#importFromMultichillViaURL ( 'wls' ) ; //'monuments_gb-wls_(en)' , 
#importFromMultichill ( 'monuments_gb-nir_(en)' , 'Northern Ireland' , 'hb' , $use_id_column ) ;


if ( true ) {
	$has_main_prop = array() ;
	$sql = "SELECT distinct epp_entity_id FROM wb_entity_per_page,pagelinks where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=120 and pl_title='P$main_prop' and pl_from_namespace=0" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$has_main_prop[$o->epp_entity_id] = 1 ;
	}

	//print_r ( $has_main_prop ) ; exit(0);


	$sql = "select distinct $use_id_column,name from entry where campaign='$campaign' AND not exists (select * from b2q where building_id=entry.id)" ;
	if ( $grade != '' ) $sql .= " and grade='$grade'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '."$sql\n");
	while($o = $result->fetch_object()){
		$source = "\tS143\t$source_item\n" ;
		print "CREATE\n" ;
		print "LAST\tLen\t\"" . ucwords ( strtolower ( $o->name ) ) . "\"\n" ;
		print "LAST\tP$main_prop\t\"" . $o->$use_id_column . "\"\n" ; # English Heritage list number
		print "LAST\tP31\t$lbq$source" ; # Instance of: Listed building
		print "LAST\tP17\tQ145$source" ; # Country: UK
		print "LAST\tP131\tQ26\n" ; # In admin unit NI
	}
}

?>
