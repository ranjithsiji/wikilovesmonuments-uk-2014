#!/usr/bin/php
<?PHP

require_once ( 'php/common.php' ) ;

$db = openToolDB ( 'wlmuk_p' ) ;


$main_prop = 709 ;
$country = 'Scotland' ;
$use_id_column = 'hbnum' ; // ext_id
#$grade = 'A' ; // I,II*
$grade = 'B' ; // I,II*
$source_item = 'Q111854' ;


$sql = "select entry.id AS id,name,hbnum from entry,region where entry.region=region.id and country='Scotland' AND grade='$grade' and not exists (select * from b2q where building_id=entry.id)" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$url = "$wdq_internal_url?q=string[$main_prop:\"" . $o->hbnum . "\"]&props=$main_prop" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !isset($j->props->$main_prop) ) continue ;
	if ( count($j->props->$main_prop) == 0 ) return ;
	foreach ( $j->props->$main_prop AS $v ) {
//		print $o->id . "\t" . $v[0] . "\t" . $v[2] . "\n" ;
		
		$q = $v[0] ;
		$id = $o->id ;

		$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES ($q,$q)" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");

		$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES ($q,$id,'wikidata')" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");
//		print "$sql\n" ; exit ( 0 ) ;
	}
}

exit ( 0 ) ;


$url = "$wdq_internal_url?q=claim[$main_prop]&props=$main_prop" ;
$j = json_decode ( file_get_contents ( $url ) ) ;

$tmp = array() ;
foreach ( $j->props->$main_prop AS $v ) {
	$tmp[$v[0]] = $v[2] * 1 ; // $db->real_escape_string ( 
}

print count ( $tmp ) . "\n" ;

$has_q = array() ;
$sql = "SELECT wikidata_id FROM b2q WHERE wikidata_id IN (" . implode(',',array_keys($tmp)) . ")" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$has_q[$o->wikidata_id] = 1 ;
	unset ( $tmp[$o->wikidata_id] ) ;
}


print count ( $tmp ) . "\n" ;




$dbwd = openDB ( 'wikidata' , '' ) ;

$onwd = array() ;
$sql = "SELECT term_entity_id,term_text FROM wb_terms WHERE term_entity_id IN (" . implode(',',array_keys($tmp)) . ") AND term_entity_type='item' AND term_language='en' AND term_type='label'" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
while($o = $result->fetch_object()){
	$pid = $tmp[$o->term_entity_id] ;
	$onwd[$pid][strtolower($o->term_text)] = $o->term_entity_id ;
//	print "$pid\t" . $o->term_entity_id . "\t" . $o->term_text . "\n" ;
}

//print_r ( $onwd ) ;

$sql = "SELECT * FROM entry WHERE hbnum IN (" . implode(',',$tmp) . ")" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$ln = trim ( strtolower($o->name) ) ;
	if ( !isset ( $onwd[$o->hbnum] ) ) {
//		print "1\n" ;
		continue ;
	}
	if ( !isset ( $onwd[$o->hbnum][$ln] ) ) {
		print "Not on WD: " . $o->name . "\n" ;
		continue ;
	}
//	print "3\n" ;

	$q = $onwd[$o->hbnum][$ln] ;
	$id = $o->id ;
	
	$sql = "INSERT IGNORE INTO wikidata (id,q) VALUES ($q,$q)" ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");

	$sql = "INSERT IGNORE INTO b2q (wikidata_id,building_id,source) VALUES ($q,$id,'wikidata')" ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."$sql\n");
//	print "$sql\n" ; exit ( 0 ) ;
}


/*
$data = array() ;
$sql = "select $use_id_column,wikidata_id,source,name,entry.id AS entry_id from entry,region,b2q where b2q.building_id=entry.id and entry.region=region.id and region.country='$country'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$data[$o->$use_id_column][strtolower($o->name)] = $o ;
}
*/


?>